// alert("test

let number = Number(prompt("Give me a number"));

console.log(`The number you provide is ${number}`);

for (let i = number; i >= 0; i--) {

	if (i <= 50) {
		console.log(`The current value is at ${i}. Terminating the loop.`);
		break;
	}

	if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (i % 5 === 0) {
		console.log(i);
		continue;
	}
}